const sphere = require("./Day 13/sphere");
const cube = require("./Day 13/cube");

const sphere1 = new sphere(7);
sphere1.whoAmI();
console.log(sphere1.calculateVolume("Fuad"));
console.log(sphere1.calculateSurface("Fuad"));

const cube1 = new cube(3);
cube1.whoAmI();
console.log(cube1.calculateVolume("Fuad"));
console.log(cube1.calculateSurface("Fuad"));
