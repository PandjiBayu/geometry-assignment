# Object Oriented Programming Assignment

We use geometry class and it's child to create new instance which is our last geometry function

---

Team member:

- Kimbrian Marshall (Rectangle & Triangle)
- Gema Darmawan (Cone & Tube)
- Faizul Fuadi (Cube & Sphere)
- Pandji Bayu (3D class & Beam)
