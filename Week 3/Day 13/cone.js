const ThreeDimension = require("./threeDimension");

class Cone extends ThreeDimension {
  constructor(radius, height) {
    super("Cone");
    this.radius = radius;
    this.height = height;
  }

  calculateCircumference() {
    super.calculateCircumference();
    return `${2 * Math.PI * this.radius * this.height} cm2`;
  }
  calculateVolume() {
    super.calculateVolume();
    return `${(1 / 3) * Math.PI * this.radius ** 2 * this.height} cm3`;
  }
}

module.exports = Cone;
