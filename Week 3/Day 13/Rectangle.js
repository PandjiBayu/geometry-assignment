const TwoDimension = require("./TwoDimension");

class Rectangle extends TwoDimension {
	constructor(width, length) {
		super("Rectangle");
		this.width = width;
		this.length = length;
	}

	// Overridding
	whatWeAre() {
		this.Type();
	}

	Type() {
		super.Type();
		console.log(`I am ${this.name}`);
	}

	// Overloading
	calculateCircumference(whoAreYou) {
		super.calculateCircumference();
		const yourName = `${whoAreYou} is trying to calculate circumference:`;
		console.log(`${yourName} ${2 * (this.width + this.length)} cm`);
		return 2 * (this.width + this.length);
	}

	calculateArea(whoAreYou) {
		super.calculateArea();
		const yourName = `${whoAreYou} is trying to calculate area:`;
		console.log(`${yourName} ${this.width * this.length} cm2`);
		return this.width * this.length;
	}
}

module.exports = Rectangle;
